const lengthValidation = (data) => {
    return data !== undefined && data.length > 0
}

const emailValidation = (email) => {
    // eslint-disable-next-line
    const testEmail = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
    let valEmail = testEmail.test(email)
    return valEmail
}

const validateData = (name, email, number) =>{
     if(lengthValidation(email) && lengthValidation(name) && lengthValidation(number)){
        if(emailValidation(email)) return true
        else return "Email"
     } else  return "Length"
 }

export {
    lengthValidation,
    emailValidation,
    validateData
}
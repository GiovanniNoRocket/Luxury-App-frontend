
import { dataCars } from "../assets/cars/carsDetails.js";

const carsContainer = document.getElementById("carsContainer");

const drawCars = (dataCars) => {

    dataCars.forEach((element) => {

        if (document.documentElement.lang == "es") {

            carsContainer.innerHTML += `      
    <!-- Column -->
        <li class="w-full my-1 px-1 lg:my-4 lg:px-4">
        <!-- Article -->
        <article class="overflow-hidden rounded-lg shadow-lg">
            <div class="w-full h-60 overflow-hidden rounded-md bg-gray-200 lg:aspect-none group-hover:opacity-75">
                <img src="${element.images[0].img}" alt="A car" class="h-full w-full object-cover object-center lg:h-full lg:w-full">
            </div>

            <header class="flex items-center justify-between leading-tight p-2 md:p-4">
                <h1 class="text-lg">
                    <a class="no-underline hover:underline text-black" href="#">
                        ${element.name}
                    </a>
                </h1>
            </header>

           <section class="mb-2 p-2 flex justify-between py-1 text-sm sm:text-base">

                <div class="items-center">
                <span class="flex class="items-center aspect-h-1 aspect-w-1" >
                    <svg fill="none" viewBox="0 0 24 24" stroke="currentColor"  style="width: 24px; height: 24px;">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z"></path>
                    </svg>
                    <span class="font-bold text-lg">${element.capacity}</span>
                    </span>
                    <p class="text-slate-500 text-sm font-medium">Capacidad</p>
                </div>
                        <div class="flex flex-col items-center">
                            <p class="text-slate-700 mb-1 text-xl font-bold">${element.model}</p>
                            <p class="text-slate-500 text-sm font-medium">Modelo</p>
                        </div>
            </section>

            <footer class="flex items-center justify-between leading-none p-2 md:p-4">
                    <a class="flex items-center no-underline hover:underline text-black" href="./carInformation.html#${element.name}">
                        <p class="">Ver Detalles</p>
                    </a>
            </footer>
        </article>
        <!-- END Article -->
    </li>
    <!-- END Column -->
            `
        } else {
            if (document.documentElement.lang == "en") {
                carsContainer.innerHTML += `      
    <!-- Column -->
        <li class="w-full my-1 px-1 lg:my-4 lg:px-4">
        <!-- Article -->
        <article class="overflow-hidden rounded-lg shadow-lg">
            <div class="w-full h-60 overflow-hidden rounded-md bg-gray-200 lg:aspect-none group-hover:opacity-75">
                <img src="${element.images[0].img}" alt="A car" class="h-full w-full object-cover object-center lg:h-full lg:w-full">
            </div>

            <header class="flex items-center justify-between leading-tight p-2 md:p-4">
                <h1 class="text-lg">
                    <a class="no-underline hover:underline text-black" href="#">
                        ${element.name}
                    </a>
                </h1>
            </header>

           <section class="mb-2 p-2 flex justify-between py-1 text-sm sm:text-base">

                <div class="items-center">
                <span class="flex class="items-center aspect-h-1 aspect-w-1" >
                    <svg fill="none" viewBox="0 0 24 24" stroke="currentColor"  style="width: 24px; height: 24px;">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z"></path>
                    </svg>
                    <span class="font-bold text-lg">${element.capacity}</span>
                    </span>
                    <p class="text-slate-500 text-sm font-medium">Capacity</p>
                </div>
                        <div class="flex flex-col items-center">
                            <p class="text-slate-700 mb-1 text-xl font-bold">${element.model}</p>
                            <p class="text-slate-500 text-sm font-medium">Model</p>
                        </div>
            </section>

            <footer class="flex items-center justify-between leading-none p-2 md:p-4">
                    <a class="flex items-center no-underline hover:underline text-black" href="/carInformation.html#${element.name}">
                        <p class="">see details</p>
                    </a>
            </footer>
        </article>
        <!-- END Article -->
    </li>
    <!-- END Column -->
            `;
            }
        }
    });
}

drawCars(dataCars);
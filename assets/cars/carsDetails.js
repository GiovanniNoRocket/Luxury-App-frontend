const dataCars =
    [
        {
            "name": "Van Mercedes Benz",
            "capacity": "19",
            "model": "2023",
            "amenities": ["Sound", "TV", "High Roof", "LED", "Air Conditioner", "Reclining seats"],
            "images": [
                {
                    "img": "../assets/images/cars/VanMercedez/VanMercedez_1.jpg"
                },
                {
                    "img": "../assets/images/cars/VanMercedez/VanMercedez_2.jpg"
                },
                {
                    "img": "../assets/images/cars/VanMercedez/VanMercedez_3.jpg"
                },
                {
                    "img": "../assets/images/cars/VanMercedez/VanMercedez_4.jpg"
                },
                {
                    "img": "../assets/images/cars/VanMercedez/VanMercedez_5.jpg"
                },
                {
                    "img": "../assets/images/cars/VanMercedez/VanMercedez_6.jpg"
                }
            ]
        },
        {
            "name": "VAN MERCEDES BENZ VITO TOURER 111",
            "capacity": "8",
            "model": "2020",
            "amenities": ["Sound", "Air Aconditioner"],
            "images": [
                {
                    "img": "../assets/images/cars/VanMercedez/VanMercedez_1.jpg"
                },
                {
                    "img": "../assets/images/cars/VanMercedez/VanMercedez_2.jpg"
                },
                {
                    "img": "../assets/images/cars/VanMercedez/VanMercedez_3.jpg"
                },
                {
                    "img": "../assets/images/cars/VanMercedez/VanMercedez_4.jpg"
                },
                {
                    "img": "../assets/images/cars/VanMercedez/VanMercedez_5.jpg"
                },
                {
                    "img": "../assets/images/cars/VanMercedez/VanMercedez_6.jpg"
                }
            ]
        },
        {
            "name": "MITSUBISHI MONTERO",
            "capacity": "7",
            "model": "2023",
            "amenities": ["Sound", "Air Aconditioner"],
            "images": [
                {
                    "img": "../assets/images/cars/VanMercedez/VanMercedez_1.jpg"
                },
                {
                    "img": "../assets/images/cars/VanMercedez/VanMercedez_2.jpg"
                },
                {
                    "img": "../assets/images/cars/VanMercedez/VanMercedez_3.jpg"
                },
                {
                    "img": "../assets/images/cars/VanMercedez/VanMercedez_4.jpg"
                },
                {
                    "img": "../assets/images/cars/VanMercedez/VanMercedez_5.jpg"
                },
                {
                    "img": "../assets/images/cars/VanMercedez/VanMercedez_6.jpg"
                }
            ]
        }
    ]

export { dataCars }
const boatsContainer = document.getElementById("boatsContainer")
const boat = document.getElementById("boat")

const drawBoats = (data, season) => {

    //Valida si el nodo esta vacio para agregar el atributo selected por defecto que sera un texto que dice no boat

   if (boat) {
        while (boat.firstChild) {
            boat.removeChild(boat.firstChild);
        }
    }

    data.forEach((element) => {

        if (document.documentElement.lang == "es") {

            boatsContainer.innerHTML += `        
    <!-- Column -->
        <li class="w-full my-1 px-1 lg:my-4 lg:px-4">
        <!-- Article -->
        <article class="overflow-hidden rounded-lg shadow-lg">
            <div class="w-full h-60 overflow-hidden rounded-md bg-gray-200 lg:aspect-none group-hover:opacity-75">
                <img src="${element.images[0].img}" alt="Front of men&#039;s Basic Tee in black." class="h-full w-full object-cover object-center lg:h-full lg:w-full">
            </div>

            <header class="flex items-center justify-between leading-tight p-2 md:p-4">
                <h1 class="text-lg">
                    <a class="no-underline hover:underline text-black" href="#">
                        ${element.name}
                    </a>
                </h1>
                <p class="text-grey-darker capitalize text-lg">${element.type=="boat" ? "Lancha": element.type=="yacht" ? "Yates" : element.type}</p>
            </header>

           <section class="mb-2 p-2 flex justify-between py-1 text-sm sm:text-base">

                <div class="items-center">
                <span class="flex class="items-center aspect-h-1 aspect-w-1" >
                    <svg fill="none" viewBox="0 0 24 24" stroke="currentColor"  style="width: 24px; height: 24px;">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z"></path>
                    </svg>
                    <span class="font-bold text-lg">${element.capacity}</span>
                    </span>
                    <p class="text-slate-500 text-sm font-medium">Capacidad</p>
                </div>
                        <div class="flex flex-col items-center">
                            <p class="text-slate-700 mb-1 text-xl font-bold">${element.size}</p>
                            <p class="text-slate-500 text-sm font-medium">Tamaño</p>
                        </div>

                <div class="flex flex-col items-center">
                    <p class="text-slate-700 mb-1 text-xl font-bold money"> ${season === "LOW" ? element.priceN : element.priceT} </p>
                    <p class="text-slate-500 text-sm font-medium">Precio</p>
                </div>

            </section>
            <footer class="flex items-center justify-between leading-none p-2 md:p-4">
                        <a class="flex items-center no-underline hover:underline text-black" href="/es/boatInformation.html#${element.name}">
                            <p class="">Ver detalles</p>
                        </a>
                        ${window.location.pathname !== '/es/nauticalCharter.html' ? `<button onclick="createReservation('${element.name}', '${season === "LOW" ? element.priceN : element.priceT}')" class="no-underline text-grey-darker hover:text-red-dark">
                        Reservar
                    </button>` : "" }
                        
                    </footer>
        </article>
        <!-- END Article -->
    </li>
    <!-- END Column -->
    `
        } else {
            if (document.documentElement.lang == "en") {
                boatsContainer.innerHTML += `
                    <!-- Column -->
            <li class="my-1 px-1 lg:my-4 lg:px-2">
                <!-- Article -->
                <article class="overflow-hidden rounded-lg shadow-lg">
                    
                    <div class="w-full h-60 overflow-hidden rounded-md bg-gray-200 lg:aspect-none group-hover:opacity-75">
                        <img src="${element.images[0].img}" alt="Front of men&#039;s Basic Tee in black." class="h-full w-full object-cover object-center lg:h-full lg:w-full">
                    </div>

                    <header class="flex items-center justify-between leading-tight p-2 md:p-4">
                        <h1 class="text-lg">
                            <a class="no-underline hover:underline text-black" href="#">
                            ${element.name}
                            </a>
                        </h1>
                        <p class="text-grey-darker capitalize text-lg">${element.type}</p>
                    </header>
            
                    <section class="mb-2 p-2 flex justify-between py-1 text-sm sm:text-base">
                        <div class="items-center">
                            <span class="flex class="items-center aspect-h-1 aspect-w-1" >
                                <svg fill="none" viewBox="0 0 24 24" stroke="currentColor"  style="width: 24px; height: 24px;">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z"></path>
                                </svg>
                                <span class="font-bold text-lg">${element.capacity}</span>
                            </span>
                            <p class="text-slate-500 text-sm font-medium">Capacity</p>
                        </div>
                        <div class="flex flex-col items-center">
                            <p class="text-slate-700 mb-1 text-xl font-bold">${element.size}</p>
                            <p class="text-slate-500 text-sm font-medium">Size</p>
                        </div>

                        <div class="flex flex-col items-center">
                            <p class="text-slate-700 mb-1 text-xl font-bold money"> ${season === "LOW" ? element.priceN : element.priceT} </p>
                            <p class="text-slate-500 text-sm font-medium">Price</p>
                        </div>

                    </section>
            
                    <footer class="flex items-center justify-between leading-none p-2 md:p-4">
                        <a class="flex items-center no-underline hover:underline text-black" href="/boatInformation.html#${element.name}">
                            <p class="">See more details</p>
                        </a>
                        ${window.location.pathname !== '/nauticalCharter.html' ? `<button onclick="createReservation('${element.name}', '${season === 'LOW' ? element.priceN : element.priceT}')" class="no-underline text-grey-darker hover:text-red-dark">
                        Book
                    </button>` : "" }
                        
                    </footer>
                </article>
                <!-- END Article -->
            </li>
            <!-- END Column --> 
            `;
            }
        }

        if (boat) {
            //Valida si el nodo esta vacio para agregar el atributo selected por defecto que sera un texto que dice no boat
            if (boat.firstChild == null) {
                const node = document.createElement("option");
                node.setAttribute("selected", "selected")
                node.setAttribute("value", "no boat")
                const textnode = document.createTextNode("No boat");
                node.appendChild(textnode)
                boat.appendChild(node)
            }
            const node = document.createElement("option");
            const textnode = document.createTextNode(element.name);
            node.appendChild(textnode)
            boat.appendChild(node)
        }


    });
}



export {
    drawBoats
}
import {
    drawBoats
} from "./generate-boats.js"
import {
    FetchData
} from "./fetch-data.js"
import {
    createCotization
} from "./make-cotization.js"

import { data } from "../assets/boats/boatsDetails.js"

const BACKENDURL = 'https://wild-teal-gharial-hose.cyclic.app'
const priceCotizationBoat = document.getElementById("priceCotizationBoat")
const priceCotizationProducts = document.getElementById("priceCotizationProducts")
const requestCotization = document.getElementById("requestCotization")
const boat = document.getElementById("boat")
const passengers = document.getElementById("passengers")
const filterByPrice = document.getElementById("filterByPrice")
const boatsCotainer = document.getElementById("boatsContainer")
const filterByType = document.getElementById("filterByType")
const dateCotization = document.getElementById("date")
const productList = document.getElementById('productsListCotization');
const filterByFt = document.getElementById("filterByFt")

//Season prices switching
// const SEASON = "LOW"
const SEASON = "HIGH"


drawBoats(data,SEASON)


if(localStorage.getItem("date") && localStorage.getItem("passengers")){
    date.value = localStorage.getItem("date")
    passengers.value = localStorage.getItem("passengers")

    localStorage.removeItem("date")
    localStorage.removeItem("passengers")
}


if(boat){

boat.addEventListener("change", () => {
    const priceOfBoat = data.find((el) => el.name === boat.value)

    if(SEASON === "HIGH")
        priceCotizationBoat.innerText = priceOfBoat.priceT
    else if(SEASON ==="LOW")
        priceCotizationBoat.innerText = priceOfBoat.priceN
    
    updateTotalPrice()
})

}

if (requestCotization) {
    requestCotization.addEventListener("submit", (e) => {
        e.preventDefault()
        const boatCotization = data.find((el) => el.name === boat.value)
        
        if (parseInt(boatCotization.capacity) >= passengers.value) {
            let productsCotization = priceCotizationProducts.textContent.replace(/[^\d]/g, "")
            let productsListInfo = []

            productList.childNodes.forEach((product, id) => {
                if (id > 0) productsListInfo.push(product.textContent)
            })

            //Creating a cotization
            createCotization(boatCotization, passengers.value, dateCotization.value, BACKENDURL, productsCotization, productsListInfo, SEASON)
        
        } else {
            if (document.documentElement.lang == "es")
                alert("La cantidad de pasajeros excede la capacidad del bote")
            else alert("The number of passengers exceeds the capacity of the boat")
        }
    })
}

filterByPrice.addEventListener("change", () => {

    if (filterByPrice.value === 'LtoM') {
        let sortedData = data.sort((a, b) => a.priceCOP - b.priceCOP)
        if (boatsCotainer)
            boatsCotainer.innerHTML = ""
        drawBoats(sortedData, SEASON)

    } else {
        let sortedData = data.sort((a, b) => a.priceCOP - b.priceCOP).reverse()
        if (boatsCotainer)
            boatsCotainer.innerHTML = ""
        drawBoats(sortedData, SEASON)
    }

    formatMoney()
})


filterByType.addEventListener("change", () => {

    if (filterByType.value === "yatchs") {
        let sortedData = data.filter((el) => el.type === "yacht")
        if (boatsCotainer)
            boatsCotainer.innerHTML = ""
        drawBoats(sortedData, SEASON)
    } else if (filterByType.value === "boats") {
        let sortedData = data.filter((el) => el.type === "boat")
        if (boatsCotainer)
            boatsCotainer.innerHTML = ""
        drawBoats(sortedData, SEASON)
    }
    else if(filterByType.value === "catamaran"){
        let sortedData = data.filter((el) => el.type === "catamaran")
        if (boatsCotainer)
            boatsCotainer.innerHTML = ""
        drawBoats(sortedData, SEASON)
    }

    else {
        if (boatsCotainer)
            boatsCotainer.innerHTML = ""
        drawBoats(data, SEASON)
    }
    formatMoney()
})

filterByFt.addEventListener("change", (e)=>{

    if (filterByFt.value === 'LtoM') {
        let sortedData = data.sort((a, b) => parseInt(a.size) - parseInt(b.size))
        if (boatsCotainer)
            boatsCotainer.innerHTML = ""
        console.log(sortedData)
        drawBoats(sortedData, SEASON)
    } else {
        let sortedData = data.sort((a, b) => parseInt(a.size) - parseInt(b.size)).reverse()
        if (boatsCotainer)
            boatsCotainer.innerHTML = ""
        drawBoats(sortedData, SEASON)
    }

    formatMoney()
})

formatMoney()

function formatMoney() {
    const money = document.querySelectorAll(".money")
    money.forEach((el) => {
        el.innerText = new Intl.NumberFormat("en", {
            style: "currency",
            currency: "COP",
            minimumFractionDigits: 0,
        }).format(el.innerText)
    })
}


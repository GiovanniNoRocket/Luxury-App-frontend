const productsView = document.getElementById("productsView")
const productsContainer = document.getElementById("productsContainer")
const boatsView = document.getElementById("boatsView")
const productsButton = document.getElementById("productsButton")
const boatsButton = document.getElementById("boatsButton")
const boat = document.getElementById("boat")
const priceCotizationBoat = document.getElementById("priceCotizationBoat")
const priceCotizationProducts = document.getElementById("priceCotizationProducts")
const productList = document.getElementById('productsListCotization');

function createReservation(name, price) {
    boat.value = name
    priceCotizationBoat.textContent = parseCurrencyFloat(parseFloat(price))
    updateTotalPrice()
}

const products = [
    {
        name: document.documentElement.lang == "en" ? "DJ sound combo, 2 booths and floor":"Combo de sonido DJ, 2 cabinas y planta",
        image: "../assets/images/products/product (1).jpeg",
        price: "1170000"
    },
    {
        name: "Six pack Coronita",
        image: "../assets/images/products/product (2).jpeg",
        price: 30000
    },
    {
        name: "Six pack Corona",
        image: "../assets/images/products/product (3).jpeg",
        price: 40000
    },
    {
        name: "Champagne Brut Rose",
        image: "../assets/images/products/product (4).jpeg",
        price: 735000
    },

    {
        name: "Champagne Ice Imperial",
        image: "../assets/images/products/product (5).jpeg",
        price: 725000
    },

    {
        name: document.documentElement.lang == "en" ? "Cheese Table":"Tabla de quesos",
        image: "../assets/images/products/product (6).jpeg",
        price: 145000
    },
    {
        name: document.documentElement.lang == "en" ? "Water Cristal 24 units" : "Agua Cristal 24 unidades",
        image: "../assets/images/products/product (7).jpeg",
        price: 27000
    },
    {
        name: "Coca Cola 250ml X12",
        image: "../assets/images/products/product (8).jpeg",
        price: 30000
    },
    {
        name: "Hatsu Pack X12",
        image: "../assets/images/products/product (9).jpeg",
        price: 36000
    },
    {
        name: document.documentElement.lang == "en" ? "Snacks X12 units" : "Mecatos Paquete X12",
        image: "../assets/images/products/product (10).jpeg",
        price: 28000
    },
    {
        name: "Bom Bom Bum X24",
        image: "../assets/images/products/product (11).jpeg",
        price: 16000
    },
    {
        name: "Chips monterojo Lemon/BBQ 100gr",
        image: "../assets/images/products/product (12).jpeg",
        price: 19000
    },
    {
        name: "Snorkel Kit",
        image: "../assets/images/products/product (13).jpeg",
        price: 48000
    },
]

products.forEach((element) => {

    if (document.documentElement.lang == "es") {
        productsContainer.innerHTML += `
        <!-- Column -->
        <li class="my-1 px-1">
        <!-- Article -->
        <article  id="product" class="overflow-hidden rounded-lg shadow-lg">
            <img alt="Placeholder" class="block object-scale-down h-60 max-w-100 mx-auto" src="${element.image}" />
            <div class="flex items-center justify-center leading-tight p-2 md:p-4">
                <div class="text-lg">
                    <a class="no-underline hover:underline text-black productName" href="#">
                        ${element.name}
                    </a>
                    <p id="priceProduct" class="text-slate-700 text-center mb-1 text-l font-bold money">${element.price}</p>
                </div>
            </div>

            <footer class="flex items-center justify-between leading-none p-2 md:p-4">
                <div class="border-2 border-gray-300 w-full h-25 flex items-center justify-center rounded-lg">
                    <button class="h-full w-1/4 bg-teal-500 border-none text-2xl rounded-lg rounded-r-none px-1 font-bold text-white" onclick="handleMinus(event)">-</button>
                    <input type="number" class="w-1/2 border-none text-xl text-center bg-gray-50" value="0" min="0" max="999" />
                    <button class="h-full w-1/4 bg-teal-500 border-none rounded-lg rounded-l-none text-2xl  px-1 font-bold text-white" onclick="handlePlus(event)">+</button>
                </div>
            </footer>
        </article>
        <!-- END Article -->
    </li>
    <!-- END Column -->
        `
    }
    else {
        if (document.documentElement.lang == "en") {
            productsContainer.innerHTML += `
    <!-- Column -->
        <li class="my-1 px-1">
        <!-- Article -->
        <article  id="product" class="overflow-hidden rounded-lg shadow-lg">
            <img alt="Placeholder" class="block object-scale-down h-60 max-w-100 " src="${element.image}" />
            <div class="flex items-center justify-center leading-tight p-2 md:p-4">
                <div class="text-lg">
                    <a class="no-underline hover:underline text-black productName" href="#">
                        ${element.name}
                    </a>
                    <p id="priceProduct" class="text-slate-700 text-center mb-1 text-l font-bold money">${element.price}</p>
                </div>
            </div>

            <footer class="flex items-center justify-between leading-none p-2 md:p-4">
                <div class="border-2 border-gray-300 w-full h-25 flex items-center justify-center rounded-lg">
                    <button class="h-full w-1/4 bg-teal-500 border-none text-2xl rounded-lg rounded-r-none px-1 font-bold text-white" onclick="handleMinus(event)">-</button>
                    <input type="number" class="w-1/2 border-none text-xl text-center bg-gray-50" value="0" min="0" max="999" />
                    <button class="h-full w-1/4 bg-teal-500 border-none rounded-lg rounded-l-none text-2xl  px-1 font-bold text-white" onclick="handlePlus(event)">+</button>
                </div>
            </footer>
        </article>
        <!-- END Article -->
    </li>
    <!-- END Column -->
    `
        }

    }
})


productsButton.addEventListener('click', (e) => {
    productsButton.classList.add("activeButton")
    boatsButton.classList.remove("activeButton")
    productsView.classList.remove("hidden")
    boatsView.classList.add("hidden")


})

boatsButton.addEventListener('click', (e) => {
    boatsButton.classList.add("activeButton")
    productsButton.classList.remove("activeButton")
    boatsView.classList.remove("hidden")
    productsView.classList.add("hidden")

})

handlePlus = (event) => {
    const button = event.target
    const product = button.closest('#product')
    const productName = product.querySelector('div a');

    const price = products.find((element) => element.name == productName.innerText).price

    const quantityElement = event.target.previousElementSibling;

    let quantity = parseFloat(quantityElement.value);
    if (quantity < 0) {
        quantity = 0;
    }
    quantity++;
    quantityElement.value = quantity;
    const existingProduct = productList.querySelector(`p[data-name="${productName.innerText}"]`)
    if (existingProduct) {
        existingProduct.innerText = `${quantityElement.value} ${productName.innerText} = ${parseCurrencyFloat(quantity * price)}`;
    }
    else {
        const productNew = document.createElement('p');
        productNew.setAttribute('data-name', `${productName.innerText}`);
        productNew.setAttribute('class', 'text-slate-700 text-center mb-1 text-sm font-bold');
        productNew.innerText = `${quantityElement.value} ${productName.innerText} = COP ${(quantity * price).toLocaleString("en")}`;
        productList.appendChild(productNew);
    }
    if (priceCotizationProducts.textContent == '') {
        priceCotizationProducts.textContent = parseFloat(price)
    }
    else {
        let newPrice = parseFloatCurrency(priceCotizationProducts.textContent) + parseFloat(price)
        priceCotizationProducts.textContent = parseCurrencyFloat(newPrice)
    }
    updateTotalPrice()
}

//Restar producto a la cotizacion y restar al total
handleMinus = (event) => {
    const button = event.target
    const product = button.closest('#product')
    const productName = product.querySelector('div a');

    const quantityElement = event.target.nextElementSibling;

    const price = products.find((element) => element.name == productName.innerText).price

    let quantity = parseFloat(quantityElement.value);
    if (quantity <= 0) {
        quantity = 0;
    }
    else {
        quantity--;
    }
    quantityElement.value = quantity;
    const existingProduct = productList.querySelector(`p[data-name="${productName.innerText}"]`)
    if (existingProduct) {
        if (quantity == 0) {
            priceCotizationProducts.textContent = `${parseCurrencyFloat(parseFloatCurrency(priceCotizationProducts.textContent) - parseFloat(price))}`
            existingProduct.remove();
        }
        else {
            existingProduct.innerText = `${quantityElement.value} ${productName.innerText} = $${quantity * price}`;
            if (priceCotizationProducts.textContent == '') {
                priceCotizationProducts.textContent = parseFloat(price)
            }
            else {
                let newPrice = parseFloatCurrency(priceCotizationProducts.textContent) - parseFloat(price)
                priceCotizationProducts.textContent = parseCurrencyFloat(newPrice)
            }
        }
    }
    updateTotalPrice()
}


updateTotalPrice = () => {
    const boatPrice = parseFloat(priceCotizationBoat.textContent.replace(/[^\d]/g, ""))
    const productsPrice = parseFloat(priceCotizationProducts.textContent.replace(/[^\d]/g, ""))
    console.log(boatPrice)
    console.log(productsPrice)
    let totalPrice = boatPrice + productsPrice 
    if (totalPrice <= 0) {
        totalPrice = 0
        priceCotizationBoat.textContent = parseCurrencyFloat(0)
        priceCotizationProducts.textContent = parseCurrencyFloat(0)
    }
    const totalPriceElement = document.getElementById("priceCotization")
    totalPriceElement.innerText = parseCurrencyFloat(totalPrice)
}

function parseFloatCurrency(value) {
    return parseFloat(value.replace(/[^\d]/g, ""))
}

function parseCurrencyFloat(value) {
    return `COP ${value.toLocaleString("en")}`
}

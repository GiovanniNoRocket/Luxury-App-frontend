import { validateData } from "./validations.js";

let informationClient = {};
const nav = document.getElementById("navBar")

const createCotization = (boatData, passengers, dateCotization, URLBACKEND, productsPrice, productsList, season) => {

    nav.classList.add("hidden")

    const priceBoat = season === "LOW" ? boatData.priceN : boatData.priceT

    const productsInfo = productsList.join(" , ")

    const modalSpanish = `
    <div class="text-left w-full" style="display: flex; justify-content: space-between; flex-wrap:wrap;">
    <div class="border p-2 shadow-lg" style="width: 100%;"> 
        <img alt="Placeholder" class="max-h-52" style="width:70%; margin:auto;" src="${boatData.images[0].img}"/>
        <p class="mt-1">Boat name: ${boatData.name}</p>
        <p >Capacidad: ${boatData.capacity}</p>
        <p ># Pasajeros: ${passengers}</p>
        <p >Fecha de reserva: ${dateCotization}</p>
        <p >Costo total: COP ${(parseFloat(productsPrice) + parseFloat(priceBoat)).toLocaleString("en")}</p>
     </div>

    <div style="width: 100%;">
    <h3 class="text-2xl my-4">Información de contacto</h3>
        <input  class="py-2 my-1 w-full border rounded" type="text" id="clientName"  placeholder="Nombre" value="${informationClient.name || ''}">
        <input  class="py-2 my-1 w-full border rounded" type="text" id="clientEmail"  placeholder="Correo electronico" value="${informationClient.email || ''}">
        <input  class="py-2 my-1 w-full border rounded" type="number" id="clientPhone"  placeholder="Telefono" value="${informationClient.phone || ''}">
    </div>
    </div>
    `

    const modalEnglish = `
    <div class="text-left w-full" style="display: flex; justify-content: space-between; flex-wrap:wrap;">
    <div class="border p-2 shadow-lg" style="width: 100%;"> 
        <img alt="Placeholder" class="max-h-60" style="width:70%; margin:auto;" src="${boatData.images[0].img}"/>
        <p class="mt-1">Boat name: ${boatData.name}</p>
        <p >Capacity: ${boatData.capacity}</p>
        <p ># Passengers: ${passengers}</p>
        <p >Reservation Date: ${dateCotization}</p>
        <p >Total price: COP ${(parseFloat(productsPrice)+parseFloat(priceBoat)).toLocaleString("en")}</p>
     </div>

    <div style="width: 100%;">
    <h3 class="text-2xl my-4">Contact information</h3>
        <input required="true"  class="py-2 my-1 w-full border rounded" type="email" id="clientName"  placeholder="Name" value="${informationClient.name || ''}">
        <input  class="py-2 my-1 w-full border rounded" type="text" id="clientEmail"  placeholder="Email" value="${informationClient.email || ''}">
        <input  class="py-2 my-1 w-full border rounded" type="number" id="clientPhone"  placeholder="Phone number" value="${informationClient.phone || ''}">
    </div>
    </div>
    `
    Swal.fire({
        title: document.documentElement.lang == "en" ? 'Boat reservation' : 'Reservar Bote',
        html: document.documentElement.lang == "en" ? modalEnglish : modalSpanish,
        showCloseButton: true,
        showCancelButton: false,
        cancelButtonText: "Cancelar",
        focusConfirm: true,
        allowOutsideClick: false,
        confirmButtonText: document.documentElement.lang == "en" ? 'Make reservation' : 'Realizar cotización',
        preConfirm: () => {
            const name = Swal.getPopup().querySelector('#clientName').value;
            const email = Swal.getPopup().querySelector('#clientEmail').value;
            const phone = Swal.getPopup().querySelector('#clientPhone').value;
            // Save temporarily the data

            let validation = validateData(name, email, phone)

                let createCotizationBody = {
                    clientName: name,
                    clientEmail: email,
                    clientPhoneNumber: phone,
                    boat: boatData.name,
                    totalCotization: parseFloat(productsPrice) + parseFloat(priceBoat),
                    cotizationDate: dateCotization,
                    boatPrice:priceBoat,
                    productsPrice:productsPrice,
                    solicitationDate: new Date().getDate(),
                    participatingPeople: passengers,
                    products:productsInfo
                }
                console.log(createCotizationBody)
               
                // Doing POST request to the server
               
                return fetch(URLBACKEND + '/api/email', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify(createCotizationBody),
                    })
                    .then(response => {
                        if (!response.ok) {
                            throw new Error('Error en la solicitud al servidor');
                        }
                        return response.json();
                    })
                    .then(data => {
                        console.log('Respuesta del servidor:', data);
                        Swal.fire({
                            icon: 'success',
                            title: '¡Información enviada!',
                            text: 'La cotizacion ha sido realizada correctamente, pronto nos comunicaremos contigo.',
                        }).then((response) => window.location.reload());
                    })
                    .catch(error => {
                        console.error('Error:', error);
                        Swal.showValidationMessage(`Error: ${error.message}`);
                    });
           
        },
        willClose: () => {
            // Restaura los datos al cerrar el modal
            informationClient.name = Swal.getPopup().querySelector('#clientName').value;
            informationClient.phone = Swal.getPopup().querySelector('#clientPhone').value
            informationClient.email = Swal.getPopup().querySelector('#clientEmail').value;
            nav.classList.remove("hidden")
        }
    });

}

export {
    createCotization
}